<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemInfo extends Model
{
    protected $table = 'configs';
    protected $fillable = [
        'key_name',
        'name',
        'type',
        'content',
    ];
}