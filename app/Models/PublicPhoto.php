<?php

namespace App\Models;

class PublicPhoto extends ReartAPI
{
    public static function getPublicPhotos ($page = null, $limit = null)
    {
        $data = [
            'page' => $page,
            'limit' => $limit,
        ];

        $res = ReartAPI::getAPI('public/getPublicPhotos', $data);

        return $res;
    }

    public static function getPhotosByStyle($style_id = 0, $page = null, $limit = null)
    {
        $data = [
            'style_id' => $style_id,
            'page' => $page,
            'limit' => $limit,
        ];

        $res = ReartAPI::getAPI('public/getPhotosByStyle', $data);

        return $res;
    }

    public static function getPublicPostInfo($post_id = 0)
    {
        $data = [
            'post_id' => $post_id,
        ];

        $res = ReartAPI::getAPI('public/getPublicPostInfo', $data);

        return $res;
    }

    public static function getStyles($page = null, $limit = null)
    {
        $data = [
            'page' => $page,
            'limit' => $limit,
        ];

        $res = ReartAPI::getAPI('public/getStyles', $data);

        return $res;
    }

    public static function time_since($since) {
        $chunks = array(
            array(60 * 60 * 24 * 365 , 'year'),
            array(60 * 60 * 24 * 30 , 'month'),
            array(60 * 60 * 24 * 7, 'week'),
            array(60 * 60 * 24 , 'day'),
            array(60 * 60 , 'hour'),
            array(60 , 'minute'),
            array(1 , 'second')
        );

        for ($i = 0, $j = count($chunks); $i < $j; $i++) {
            $seconds = $chunks[$i][0];
            $name = $chunks[$i][1];
            if (($count = floor($since / $seconds)) != 0) {
                break;
            }
        }

        $print = ($count == 1) ? '1 '.$name : "$count {$name}s";

        return $print;
    }
}
