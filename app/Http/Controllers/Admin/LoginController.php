<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Requests\LoginRequest;

class LoginController extends Controller
{
    public function login(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {

            return redirect(route('config'));
        }
        else {
            $message = 'Email or password is not exact';

            return view('admin.login')->with('message', $message);
        }
    }

    public function showFormLogin()
    {
        return view('admin.login');
    }

    public function logout()
    {
        Auth::logout();

        return redirect(route('login'));
    }
}
