<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'home', 'uses' => 'HomeController@index'
]);

Route::get('/detail/{id}', [
    'as' => 'detail', 'uses' => 'HomeController@detail'
]);

Route::get('/style-detail/{id}', [
    'as' => 'styleDetail', 'uses' => 'HomeController@styleDetail'
]);

Route::get('admin/login', [
    'as' => 'login', 'uses' => 'Admin\LoginController@showFormLogin'
]);

Route::post('admin/login', [
    'as' => 'postLogin', 'uses' => 'Admin\LoginController@login'
]);

Route::group(['middleware' => ['admin'], 'prefix' => 'admin', 'namespace' => 'Admin'], function ()
{
    Route::get('/', [
        'as' => 'config', 'uses' => 'ConfigController@index'
    ]);

    Route::get('feature', [
        'as' => 'feature', 'uses' => 'ConfigController@feature'
    ]);

    Route::get('feature/edit/{id}', [
        'as' => 'editFeature', 'uses' => 'ConfigController@editFeature'
    ]);

    Route::get('config/edit/{id}', [
        'as' => 'editConfig', 'uses' => 'ConfigController@editConfig'
    ]);

    Route::post('feature/update/{id}', [
        'as' => 'updateFeature', 'uses' => 'ConfigController@updateFeature'
    ]);

    Route::post('config/update/{id}', [
        'as' => 'updateConfig', 'uses' => 'ConfigController@updateConfig'
    ]);

    Route::get('admin/profile', [
        'as' => 'profile', 'uses' => 'ProfileController@edit'
    ]);

    Route::post('admin/profile', [
        'as' => 'updateProfile', 'uses' => 'ProfileController@update'
    ]);

    Route::get('logout', [
        'as' => 'logout', 'uses' => 'LoginController@logout'
    ]);
});
