@extends('layout.master')

@section('content')
    <div class="modal modal-full modal-pop-img-detail" id="popImgDetail" tabindex="-1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="detail-img">
                        <div class="detail-heading">
                            <div class="fixed-column"><a class="btn btn-info btn-rounder btn-block btn-lg open-app" href="#">Open In App</a></div>
                            <div class="fluid-column">
                                <div class="inner">
                                    <div class="media">
                                        <div class="media-left"><img src="{{ $photo->avatar }}" class="img-circle" alt="sample" width="80px"></div>
                                        <div class="media-body">
                                            <div class="user-name">{{ $photo->fullname }}</div>
                                            <div class="time">{{ $since }} ago</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail-content">
                            <p>There are days like that, quietly, not sad, not happy, slowly drift… the end of a day.</p>
                            <p><a href="#">#rainday<br></a><a href="#">#sad</a></p>
                            <div class="img-layout">
                                <div class="main-img"><img class="img-responsive" src="{{ $photo->output }}" alt="sample" width="500px" height="500px"></div>
                                <div class="sub-img">
                                    <div><img class="img-responsive" src="{{ $photo->input }}" alt="sample" width="248px" height="248px"></div>
                                    <div><img class="img-responsive" src="{{ $photo->template }}" alt="sample" width="248px" height="248px"></div>
                                </div>
                            </div>
                        </div>
                        <div class="detail-footer">
                            <ul class="reset-list foot-actions">
                                <li><a href="#"><i class="icon icon-heart-o"></i><span class="likes">{{ $photo->total_like }} likes</span></a></li>
                                <li><a href="#"><i class="icon icon-comment-o"></i><span class="likes">{{ $photo->total_comment }} comments</span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
