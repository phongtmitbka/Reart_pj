@extends('layout.master')

@section('content')

    @foreach ($photos as $photo)
        <div class="col-md-12">
            <img src="{{ $photo->output }}">
        </div>
    @endforeach

@endsection
