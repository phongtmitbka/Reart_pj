@extends('admin.layout.master')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Profile
                    <small>Update</small>
                </h1>
            </div>
            <!-- /.col-lg-12 -->
            <div class="col-lg-7" style="padding-bottom:120px">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (isset($message))
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{ $message }}</li>
                        </ul>
                    </div>
                @endif
                <form action="{{ route('updateProfile') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="txtEmail" placeholder="Please Enter Email" value="{{ $profile->email }}" disabled="" />
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input class="form-control" name="txtName" placeholder="Please Enter Name" value="{{ $profile->name }}" required />
                    </div>
                    <div class="form-group">
                        <label><input type="checkbox" id="enter_pass" /> Enter Password</label>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control password" name="txtPass" placeholder="Please Enter Password" disabled />
                    </div>
                    <div class="form-group">
                        <label>RePassword</label>
                        <input type="password" class="form-control password" name="txtRePass" placeholder="Please Enter RePassword" disabled />
                    </div>
                    <button type="submit" class="btn btn-default">Update</button>
                    <button type="reset" class="btn btn-default">Reset</button>
                    <form>
            </div>
        </div>
        <!-- /.row -->
    </div>

    <script>
        $(document).ready(function() {
            $("#enter_pass").on('click', function(){
                if ($(this).is(':checked')) {
                    $('.password').removeAttr("disabled");
                } else {
                    $('.password').attr('disabled', true);
                }
            });
        });
    </script>
@endsection
