<!DOCTYPE html>
<html class="no-js" lang="en">

@include('layout.head')

    @yield('content')

@include('layout.foot')
</html>