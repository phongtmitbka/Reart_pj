<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home Page</title>
    <meta name="description" content="Page description">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <base href="{{ asset('') }}">
    <link rel="shortcut icon" href="re-art/app/ico/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" href="re-art/app/ico/apple-touch-icon.png">
    <!-- build:css css/style.min.css-->
    <link href="re-art/app/css/styles.css" rel="stylesheet" type="text/css">
    <!-- endbuild-->
</head>
