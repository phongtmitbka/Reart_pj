<header class="site-header" id="header"><span class="button-toggle" onclick="toggleMenu(this)">
        <div class="fa fa-bars"></div></span><a class="logo-mobile" href="#"><img src="re-art/app/imgs/logo.png" srcset="re-art/app/imgs/logo.png 1x, re-art/app/imgs/logo@2x.png 2x" alt="Logo Re-Art"></a>
    <div id="navbar-menu-top">
        <ul class="menu-top reset-list" role="tablist">
            <li><a href="#header">HOME</a></li>
            <li><a href="#feature-sec" onclick="closeMenu(this)">FEATURES APP</a></li>
            <li><a href="#popular-img-sec" onclick="closeMenu(this)">POPULAR IMAGES</a></li>
            <li><a href="about-us.html">ABOUT US</a></li>
            <li><a href=".more-apps.html">MORE APPS</a></li>
        </ul>
    </div>
</header>
